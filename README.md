## Usage

```js
// {app_root}/config/plugin.js
exports.rbac = {
  enable: true,
  package: 'egg-rbac-plugin',
};
```

## Configuration

config getGroupName
```js
// {app_root}/config/config.default.js
exports.rbac = {
  initOnStart: false, // default false
  multipleConnect: false, // 是否是多数据库连接方式
  dbName: '', // 如果是多数据库连接方式，则指明哪个是希望rbac使用的数据库
  /**
   * @param {object} ctx - egg context object
   * @return {object} promise, if resolve data is falsy, no role
   */
  async getGroupName(ctx) { // eslint-disable-line
    return Promise.resolve('');
  }
};
```

Initialize roles and permissions
```js
// {app_root/config/rbac.js}
'use strict';

exports.permissions = [
  // action_resource
  // { name: 'create_user', alias: '创建用户', category: 'user', categoryAlias: '用户' },
  // { name: 'delete_user', alias: '删除用户' category: 'user', categoryAlias: '用户' },
  // { name: 'query_user', alias: '查询用户' category: 'user', categoryAlias: '用户' },
  // { name: 'edit_user', alias: '修改用户' category: 'user', categoryAlias: '用户' },
];

exports.roles = [
  // { name: 'userAdmin', alias: '用户管理员', permissions: ['create_user', 'delete_user', 'query_user', 'edit_user'] },
];

exports.groups = [
  // { name: 'superAdmin', alias: '超级管理员', roles: ['userAdmin'] },
]
```


## Example



## Remarks

- It will create a superadmin role which own all permissions.

## Contacts

if you has any questions, please contact me. thanks!

E-mail: wangrenbin@126.com, laotie1015@163.com

## License

[MIT](LICENSE)