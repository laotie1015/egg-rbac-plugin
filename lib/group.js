'use strict';

const assert = require('assert');
/**
 * @class Group
 */
module.exports = exports = class Group {

  /**
   * @constructs Group
   * @param {string} groupName - group name
   * @param {object} permissionItems - singleton rbac object
   */
  constructor(groupName, permissionItems) {
    /**
     * @property {object} _permisstion
     * @private
     * @example
     * {
     *   'create_user': {
     *     _id: '12387389173248'
     *     name: 'create_user'
     *     alias: '创建用户'
     *     category: 'user'
     *     categoryAlias: '用户'
     *   }
     * }
     */
    assert(permissionItems, 'Group constructor parameter permissionItems is undefined');
    this._permissions = Group._createPermisstion(permissionItems);

    assert(groupName && typeof groupName === 'string', 'Group constructor parameter groupName is undefined');
    this._name = groupName;
  }

  /**
   * @static
   * @param {object[]} permissionItems - permission item array
   * @param {string} permissionItems[]._id - ObjectId
   * @param {string} permissionItems[].name - permission name
   * @param {string} permissionItems[].alias - permission alias
   * @return {object} - structure same to this._permission
   */
  static _createPermisstion(permissionItems) {
    const result = {};
    permissionItems.forEach(item => {
      result[item.name] = item;
    });
    return result;
  }

  /**
   * @member {string}
   */
  get groupName() {
    return this._name;
  }

  /**
   * @method Group#can
   * @param {string} permissionName - permisston name
   * @return {boolen} can or not
   */
  can(permissionName) {
    return !!this._permissions[permissionName];
  }

  /**
   * check the group grant any permission or not
   * @method Group#canAny
   * @param {string} permissionNames - permisston name
   * @return {boolen} can or not
   */
  canAny(permissionNames) {
    return permissionNames.some(item => !!this._permissions[item]);
  }

  /**
   * check the group grant all permission or not
   * @method Group#canAll
   * @param {string} permissionNames - permisston name
   * @return {boolen} can or not
   */
  canAll(permissionNames) {
    return permissionNames.every(item => !!this._permissions[item]);
  }
};
