'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const ObjectId = mongoose.Schema.ObjectId;
  const roleSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    alias: { type: String },
    permissions: [{ type: ObjectId, ref: 'permission' }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  if (app.config.rbac.multipleConnect) {
    const rbacdb = app.mongooseDB.get(app.config.rbac.dbName);
    return rbacdb.model('role', roleSchema);
  } else {
    return mongoose.model('role', roleSchema);
  }
};