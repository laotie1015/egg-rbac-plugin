'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const ObjectId = mongoose.Schema.ObjectId;
  const groupSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    alias: { type: String },
    roles: [{ type: ObjectId, ref: 'role' }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  if (app.config.rbac.multipleConnect) {
    const rbacdb = app.mongooseDB.get(app.config.rbac.dbName);
    return rbacdb.model('group', groupSchema);
  } else {
    return mongoose.model('group', groupSchema);
  }
};