'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const ObjectId = mongoose.Schema.ObjectId;
  const permissionSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true }, // 授权名称
    alias: { type: String }, // 授权的别名
    category: { type: String }, // 授权的分类, 如：order
    categoryAlias: { type: String }, // 授权分类的别名，例： 订单
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  if (app.config.rbac.multipleConnect) {
    const rbacdb = app.mongooseDB.get(app.config.rbac.dbName);
    return rbacdb.model('permission', permissionSchema);
  } else {
    return mongoose.model('permission', permissionSchema);
  }
};