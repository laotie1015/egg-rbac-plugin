'use strict';

const Group = require('../../lib/group');
const debug = require('debug')('egg-rbac-plugin')

// options: 中间件的配置项，框架会将 app.config[${middlewareName}] 传递进来
module.exports = options => {
  return async function rbac(ctx, next) {
    // console.log('rbac');
    const groupName = await options.getGroupName(ctx);
    if (groupName) {
      const permissions = await ctx.app.rbac.getGroupPermission(groupName);
      ctx.group = new Group(groupName, permissions);
    }
    await next();
  };
};
