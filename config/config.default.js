'use strict';

/**
 * egg-rbac default config
 * @member Config#rbac
 * @property {String} SOME_KEY - some description
 */
exports.rbac = {
  initOnStart: false, // default false
  multipleConnect: false, // 是否是多数据库连接方式
  /**
   * exports.mongoose = {
    // 单个数据库配置
    // client: {
    //   url: 'mongodb://127.0.0.1:27017/test',
    //   options: {
    //     useCreateIndex: true
    //   },
    // },
    // 多个数据库配置
    clients: {
      test1: {
        url: 'mongodb://127.0.0.1:27017/test1',
        options: {
          useCreateIndex: true
        },
      },
      test2: {
        url: 'mongodb://127.0.0.1:27017/test2',
        options: {
          useCreateIndex: true
        },
      }
    }
  }
   */
  dbName: '', // 例:"test1",项目mongoose配置中的数据库名称，如果是多数据库连接方式，则指明哪个是希望rbac使用的数据库
  /**
   * @param {object} ctx - egg context object
   * @return {object} promise, if resolve data is falsy, no role
   */
  async getGroupName(ctx) { // eslint-disable-line
    return Promise.resolve('');
  },
  superAdmin: {
    name: 'superAdmin',
    alias: '超级管理员',
  },
};